const { Client, GatewayIntentBits, Collection } = require("discord.js");
const dotenv = require("dotenv");
const { REST } = require("@discordjs/rest");
const { Routes } = require("discord-api-types/v9");
const fs = require("fs");
const { Player } = require("discord-player");

dotenv.config();

const TOKEN = process.env.TOKEN;
const LOAD_SLASH = process.argv[2] == "load";
const CLIENT_ID = "790626906088669184"; // Remplacez par votre Client ID
const GUILD_ID = "973179744969441300"; // Remplacez par votre Guild ID

const client = new Client({
  intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildVoiceStates],
});

client.slashcommands = new Collection();
client.player = new Player(client, {
  ytdlOptions: {
    quality: "highestaudio",
    highWaterMark: 1 << 25,
  },
});

let commands = [];

const slashFiles = fs
  .readdirSync("./slash")
  .filter((file) => file.endsWith(".js"));

for (const file of slashFiles) {
  try {
    console.log("slash files", slashFiles);
    const slashcmd = require(`./slash/${file}`);
    console.log("skashcmd", slashcmd);
    client.slashcommands.set(slashcmd.data.name, slashcmd);
    if (LOAD_SLASH) {
      console.log("enter");
      commands.push(slashcmd.data.toJSON());
    }
    console.log("commands ", commands);
  } catch (error) {
    console.error("error", error);
  }
}

if (LOAD_SLASH) {
  try {
    const rest = new REST({ version: "9" }).setToken(TOKEN);
    console.log("Deploying slash commands");
    rest
      .put(Routes.applicationGuildCommands(CLIENT_ID, GUILD_ID), {
        body: commands,
      })
      .then(() => {
        console.log("Successfully loaded");
        process.exit(0);
      })
      .catch((err) => {
        if (err) {
          console.log(err);
          process.exit(1);
        }
      });
  } catch (error) {
    console.error("error", error);
  }
} else {
  try {
    client.on("ready", () => {
      console.log(`Logged in as ${client.user.tag}`);
    });

    client.on("interactionCreate", async (interaction) => {
      if (!interaction.isCommand()) return;

      const slashcmd = client.slashcommands.get(interaction.commandName);
      if (!slashcmd) {
        await interaction.reply("Not a valid slash command");
        return;
      }

      await interaction.deferReply();
      await slashcmd.run({ client, interaction });
    });

    client.login(TOKEN);
  } catch (error) {
    console.error("error else", error);
  }
}
